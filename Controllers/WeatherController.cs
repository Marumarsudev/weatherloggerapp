﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using WeatherLoggerApp.Models;

namespace WeatherLoggerApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeatherController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public WeatherController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public JsonResult Get()
        {
            // Swap to Stored Procedure at some point in time :)
            string query = @"select WeatherId, Date, Temperature, RainAmount, WindSpeed, Place from dbo.Weather";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("WeatherAppCon");
            SqlDataReader reader;
            using(SqlConnection conn = new SqlConnection(sqlDataSource))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    reader = command.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    conn.Close();
                }
            }

            return new JsonResult(table);
        }

        [HttpPost]
        public JsonResult Post(Weather w)
        {
            // Swap to Stored Procedure at some point in time :)
            string query = @"insert into dbo.Weather values ('"+w.Date.ToString("yyyy-MM-dd HH:mm:ss.fff")+ "', "+w.Temperature+", "+w.RainAmount+", "+w.WindSpeed+ ", "+w.Place+");";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("WeatherAppCon");
            SqlDataReader reader;
            using (SqlConnection conn = new SqlConnection(sqlDataSource))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    reader = command.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    conn.Close();
                }
            }

            return new JsonResult("Added Successfully");
        }

        [HttpPut]
        public JsonResult Put(Weather w)
        {
            // Swap to Stored Procedure at some point in time :)
            string query = @"update dbo.Weather " + 
                "set Date ='" + w.Date.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'," +
                "Temperature=" + w.Temperature + "," +
                "RainAmount=" + w.RainAmount + "," +
                "WindSpeed=" + w.WindSpeed + "," +
                "Place =" + w.Place + " " +
                "where WeatherId =" + w.WeatherId;
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("WeatherAppCon");
            SqlDataReader reader;
            using (SqlConnection conn = new SqlConnection(sqlDataSource))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    reader = command.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    conn.Close();
                }
            }

            return new JsonResult("Updated Successfully");
        }

        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            // Swap to Stored Procedure at some point in time :)
            string query = @"delete from dbo.Weather where WeatherId = " + id + ";";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("WeatherAppCon");
            SqlDataReader reader;
            using (SqlConnection conn = new SqlConnection(sqlDataSource))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand(query, conn))
                {
                    reader = command.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    conn.Close();
                }
            }

            return new JsonResult("Deleted Successfully");
        }
    }
}
