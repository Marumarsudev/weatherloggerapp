﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherLoggerApp.Models
{
    public class Place
    {
        public int PlaceId { get; set; }

        public string Name { get; set; }

    }
}
