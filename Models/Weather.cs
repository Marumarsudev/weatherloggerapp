﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherLoggerApp.Models
{
    public class Weather
    {
        public int WeatherId { get; set; }

        public DateTime Date { get; set; }

        public float Temperature { get; set; }

        public float RainAmount { get; set; }

        public float WindSpeed { get; set; }

        public int Place { get; set; }

    }
}
