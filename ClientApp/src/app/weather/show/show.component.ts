import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})

export class ShowComponent implements OnInit {

  constructor(private service:SharedService) { }

  WeatherList:any = [];

  ngOnInit(): void
  {
    this.RefreshWeatherList();
  }

  
  ModalTitle:string;
  ActivateAddEditWeatherComp:boolean = false;
  w:any;
  addClick()
  {
    this.w = {
      WeatherId:0,
      Date:new Date().toISOString().substring(0, 10),
      Temperature:0,
      RainAmount:0,
      WindSpeed:0,
      Place:0
    }
    this.ModalTitle = "Add Weather Report";
    this.ActivateAddEditWeatherComp = true;
  }

  editClick(item)
  {
    this.w=item;
    this.ModalTitle="Edit Weather Report";
    this.ActivateAddEditWeatherComp=true;
  }

  closeClick()
  {
    this.ActivateAddEditWeatherComp = false;
    this.RefreshWeatherList();
  }

  deleteClick(item)
  {
    if(confirm("Are you sure?"))
    {
      this.service.DeleteWeather(item.WeatherId).subscribe(data => {
        alert(data.toString());
        this.RefreshWeatherList();
      });
    }
  }

  RefreshWeatherList()
  {
    this.service.GetWeatherList().subscribe(data => {
      data.forEach(dataItem => dataItem.PlaceName = "Unavailable");
      this.service.GetPlaceList().subscribe(pdata => {
        pdata.forEach(pdataItem => {
          data.forEach(dataItem => {
            if (pdataItem.PlaceId === dataItem.Place)
            {
              dataItem.PlaceName = pdataItem.Name;
            }
          });
        });
      });
      this.WeatherList = data;
    });
  }

}
