import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  constructor(private service:SharedService) { }

  @Input() w:any;
  WeatherId:string;
  Date:Date;
  Temperature:string;
  RainAmount:string;
  WindSpeed:string;
  Place:string;
  PlaceList:any = [];

  form = new FormGroup({
    placeform: new FormControl('0', Validators.required)
  });

  ngOnInit() {
    this.refreshPlaceList();
    this.WeatherId = this.w.WeatherId;
    this.Date = this.w.Date;
    this.Temperature = this.w.Temperature;
    this.RainAmount = this.w.RainAmount;
    this.WindSpeed = this.w.WindSpeed;
    this.Place = this.w.Place;
  }

  refreshPlaceList()
  {
    this.service.GetPlaceList().subscribe(pdata => {
      this.PlaceList = pdata;
    });
  }

  addWeatherReport()
  {
    console.log(this.form.value);
    let val = {WeatherId:this.WeatherId,
               Date:this.Date,
               Temperature:this.Temperature,
               RainAmount:this.RainAmount,
               WindSpeed:this.WindSpeed,
               Place:this.form.value.placeform};
    this.service.AddWeather(val).subscribe(res => {
      alert(res.toString());
    });
  }

  updateWeatherReport()
  {
    let val = {WeatherId:this.WeatherId,
      Date:this.Date,
      Temperature:this.Temperature,
      RainAmount:this.RainAmount,
      WindSpeed:this.WindSpeed,
      Place:this.form.value.placeform};
    this.service.UpdateWeather(val).subscribe(res => {
      alert(res.toString());
    });
  }

}
