import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  readonly APIUrl="http://localhost:5000/api/";
  constructor(private http:HttpClient) { }

  GetWeatherList():Observable<any[]>
  {
    return this.http.get<any>(this.APIUrl+"weather");
  }

  AddWeather(val:any)
  {
    return this.http.post(this.APIUrl+"weather", val);
  }

  UpdateWeather(val:any)
  {
    return this.http.put(this.APIUrl+"weather", val);
  }

  DeleteWeather(val:any)
  {
    return this.http.delete(this.APIUrl+"weather/" + val);
  }

  GetPlaceList():Observable<any[]>
  {
    return this.http.get<any>(this.APIUrl+"place");
  }

  AddPlace(val:any)
  {
    return this.http.post(this.APIUrl+"place", val);
  }

  UpdatePlace(val:any)
  {
    return this.http.put(this.APIUrl+"place", val);
  }

  DeletePlace(val:any)
  {
    return this.http.delete(this.APIUrl+"place/" + val);
  }
}
