import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { WeatherComponent } from './weather/weather.component';
import { ShowComponent } from './weather/show/show.component';
import { AddComponent } from './weather/add/add.component';
import { PlaceComponent } from './place/place.component';
import { SharedService } from './shared.service';
import { AppRoutingModule } from './app-routing.module';
import { AddComponentPlace } from './place/add/add.component';
import { ShowComponentPlace } from './place/show/show.component';

@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent,
    ShowComponent,
    AddComponent,
    PlaceComponent,
    ShowComponentPlace,
    AddComponentPlace
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([]),
    AppRoutingModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
