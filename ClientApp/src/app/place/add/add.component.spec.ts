import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddComponentPlace } from './add.component';

describe('AddComponent', () => {
  let component: AddComponentPlace;
  let fixture: ComponentFixture<AddComponentPlace>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddComponentPlace ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddComponentPlace);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
