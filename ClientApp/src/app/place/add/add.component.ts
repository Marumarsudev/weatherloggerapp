import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-place',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponentPlace implements OnInit {

  constructor(private service:SharedService) { }

  @Input() w:any;
  PlaceId:string;
  Name:string;
  PlaceList:any = [];

  ngOnInit() {
    this.PlaceId = this.w.PlaceId;
    this.Name = this.w.Name;
  }

  addPlace()
  {
    let val = {
      PlaceId:this.PlaceId,
      Name:this.Name};
    this.service.AddPlace(val).subscribe(res => {
      alert(res.toString());
    });
  }

  updatePlace()
  {
    let val = {
      PlaceId:this.PlaceId,
      Name:this.Name};
    this.service.UpdatePlace(val).subscribe(res => {
      alert(res.toString());
    });
  }

}
