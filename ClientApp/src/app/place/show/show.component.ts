import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-place',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})

export class ShowComponentPlace implements OnInit {

  constructor(private service:SharedService) { }

  PlaceList:any = [];

  ngOnInit(): void
  {
    this.RefreshPlaceList();
  }

  
  ModalTitle:string;
  ActivateAddEditPlaceComp:boolean = false;
  w:any;
  addClick()
  {
    this.w = {
      PlaceId:0,
      Name:""
    }
    this.ModalTitle = "Add a Place";
    this.ActivateAddEditPlaceComp = true;
  }

  editClick(item)
  {
    this.w=item;
    this.ModalTitle="Edit the Place";
    this.ActivateAddEditPlaceComp=true;
  }

  closeClick()
  {
    this.ActivateAddEditPlaceComp = false;
    this.RefreshPlaceList();
  }

  deleteClick(item)
  {
    if(confirm("Are you sure?"))
    {
      this.service.DeletePlace(item.PlaceId).subscribe(data => {
        alert(data.toString());
        this.RefreshPlaceList();
      });
    }
  }

  RefreshPlaceList()
  {
    this.service.GetPlaceList().subscribe(data => {
      this.PlaceList = data;
    });
  }

}
