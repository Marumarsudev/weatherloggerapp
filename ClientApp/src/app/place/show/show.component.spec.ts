import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowComponentPlace } from './show.component';

describe('ShowComponent', () => {
  let component: ShowComponentPlace;
  let fixture: ComponentFixture<ShowComponentPlace>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowComponentPlace ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowComponentPlace);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
